package se.azakir.photoadder.differ;

import java.util.ArrayList;
import java.util.List;

public class FindDifference {

    /**
     * Finds difference between two lists
     *
     * @param a   List a
     * @param b   List b
     * @param <T>
     * @return List that contains elements that only exist in a
     */
    public static <T> List<T> findDifference(List<T> a, List<T> b) {
        List<T> c = new ArrayList<>(a);
        c.removeAll(b);

        return c;
    }
}
