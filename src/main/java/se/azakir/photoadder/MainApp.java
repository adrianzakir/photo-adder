package se.azakir.photoadder;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import se.azakir.photoadder.adder.PhotoAdder;
import se.azakir.photoadder.config.AppConfig;
import se.azakir.photoadder.init.InitApp;

import java.util.Scanner;

public class MainApp {

    public static void main(String[] args) {
        System.out.println("Welcome to the photo-adder.\n");

        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        InitApp app = context.getBean(InitApp.class);
        String property = app.getProperty(InitApp.PROJECT_PATH);
        if (property == null) {
            System.out.println("Enter path to project:");
            Scanner scanner = new Scanner(System.in);
            property = scanner.nextLine();
            app.addProperty(InitApp.PROJECT_PATH, property);
        }
        PhotoAdder photoAdder = context.getBean("photoAdder", PhotoAdder.class);
        photoAdder.setDirectoryPath(property);
        photoAdder.runMainFunction();

        context.close();
    }


}
