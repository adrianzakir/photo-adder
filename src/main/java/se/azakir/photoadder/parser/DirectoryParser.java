package se.azakir.photoadder.parser;

import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Component
public class DirectoryParser {

    /**
     * Finds all image files in a folder
     *
     * @param file File that points to a folder
     * @return List of paths to the images in the folder
     */
    public List<String> parseFile(File file) {
        List<String> imageNames = new ArrayList<>();
        File[] images = file.listFiles();
        //TODO change to filefilter or use lambda
        for (File fileInDirectory : images) {
            if (fileInDirectory.getName().endsWith(".jpg") || fileInDirectory.getName().endsWith(".jpeg"))
                imageNames.add(fileInDirectory.getName());
        }

        return imageNames;
    }
}
