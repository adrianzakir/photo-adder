package se.azakir.photoadder.parser;

import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class ProjectParser {

    private static final String DIRECTORY_INDEX_FILE_PATH = "/index.html";
    private static final String DIRECTORY_IMAGES_FILE_PATH = "/images";

    private String directoryPath;
    private File htmlFile;
    private File imagesDirectory;

    public ProjectParser() {

    }

    public void init() {
        File projectDirectory = new File(directoryPath);
        if (!projectDirectory.exists()) {
            System.out.println("Project directory was not found for: " + directoryPath);
        }
        htmlFile = new File(projectDirectory.getAbsolutePath() + DIRECTORY_INDEX_FILE_PATH);
        imagesDirectory = new File(projectDirectory.getAbsolutePath() + DIRECTORY_IMAGES_FILE_PATH);
    }

    public File getHtmlFile() {
        return htmlFile;
    }

    public void setHtmlFile(File htmlFile) {
        this.htmlFile = htmlFile;
    }

    public File getImagesDirectory() {
        return imagesDirectory;
    }

    public void setImagesDirectory(File imagesDirectory) {
        this.imagesDirectory = imagesDirectory;
    }

    public String getDirectoryPath() {
        return directoryPath;
    }

    public void setDirectoryPath(String directoryPath) {
        this.directoryPath = directoryPath;
    }

    @Override
    public String toString() {
        return "ProjectParser{" +
                "directoryPath='" + directoryPath + '\'' +
                ", htmlFile=" + htmlFile +
                ", imagesDirectory=" + imagesDirectory +
                '}';
    }
}
