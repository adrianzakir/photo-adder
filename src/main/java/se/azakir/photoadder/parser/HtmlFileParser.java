package se.azakir.photoadder.parser;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
public class HtmlFileParser {

    private Document document;

    /**
     * Finds all image files in an HTML file
     *
     * @param file HTML file to be searched
     * @return List of paths to the images in the html file
     */
    public List<String> parseFile(File file) {
        List<String> imageNames = new ArrayList<>();
        try {
            Document doc = Jsoup.parse(file, StandardCharsets.UTF_8.toString());
            Elements imageElements = doc.getElementsByTag("img");
            for (Element imageElement : imageElements) {
                String src = imageElement.attr("src");
                Path path = Paths.get(src);
                imageNames.add(path.getFileName().toString());
            }
            this.setDocument(doc);

            return imageNames;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return imageNames;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
}
