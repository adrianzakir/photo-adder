package se.azakir.photoadder.adder;

import org.apache.commons.io.FileUtils;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import se.azakir.photoadder.differ.FindDifference;
import se.azakir.photoadder.parser.DirectoryParser;
import se.azakir.photoadder.parser.HtmlFileParser;
import se.azakir.photoadder.parser.ProjectParser;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Component
public class PhotoAdder {

    @Autowired
    private ProjectParser projectParser;
    @Autowired
    private DirectoryParser directoryParser;
    @Autowired
    private HtmlFileParser htmlFileParser;

    private String directoryPath;

    public void runMainFunction() {
        System.out.println("Loading project from: " + directoryPath);
        projectParser.setDirectoryPath(directoryPath);
        projectParser.init();

        List<String> htmlImages = htmlFileParser.parseFile(projectParser.getHtmlFile());
        List<String> imagesInFolder = directoryParser.parseFile(projectParser.getImagesDirectory());
        List<String> newImages = FindDifference.findDifference(imagesInFolder, htmlImages);

        System.out.println("There are " + newImages.size() + " new images.\n");

        PhotoAdderWriter photoAdderWriter = new PhotoAdderWriter(newImages, htmlFileParser.getDocument());
        Document document = photoAdderWriter.addPhotosToDocument();
        writeToHtml(document);
    }

    public String getDirectoryPath() {
        return directoryPath;
    }

    public void setDirectoryPath(String directoryPath) {
        this.directoryPath = directoryPath;
    }

    /**
     * Writes Jsoup document to HTML file.
     *
     * @param document The document that wil be inserted to the file.
     */
    public void writeToHtml(Document document) {
        System.out.println("Writing to file...");
        try {
            File f = new File(directoryPath + "/index.html");
            FileUtils.writeStringToFile(f, document.outerHtml(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("File write completed!");
    }
}
