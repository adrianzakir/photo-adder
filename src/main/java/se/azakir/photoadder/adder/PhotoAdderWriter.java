package se.azakir.photoadder.adder;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PhotoAdderWriter {

    private static final int COLUMN_SIZE = 12;

    private final List<String> newImages;
    private final Document document;
    private final Random random = new Random();

    public PhotoAdderWriter(List<String> newImages, Document document) {
        this.newImages = newImages;
        this.document = document;
    }

    /**
     * Adds images to Jsoup document
     *
     * @return Jsoup document with new images added
     */
    public Document addPhotosToDocument() {
        System.out.println("Adding images to HTML file...");
        while (!newImages.isEmpty()) {
            int randInt = random.nextInt(4) + 2;
            List<String> insertImagesInRow = newImages.stream().limit(randInt).toList();
            int[] imagePositions = positions(insertImagesInRow.size(), COLUMN_SIZE);
            for (int i = 0; i < insertImagesInRow.size(); i++) {
                Elements elements = document.getElementsByClass("container-fluid photos");
                Element element = createElement(insertImagesInRow.get(i), imagePositions[i]);
                elements.get(0).child(0).insertChildren(0, element);
            }
            newImages.removeAll(insertImagesInRow);
        }

        System.out.println("All images have been added.\n");

        return document;
    }

    /**
     * Creates image element to be inserted in the document
     *
     * @param image path to the image
     * @param size  size of the image in regard to bootstrap column size
     * @return element to be inserted into the document
     */
    private Element createElement(String image, int size) {
        Element divElement = new Element("div");
        divElement.attr("class", "col-6 col-md-6 col-lg-" + size);
        divElement.attr("data-aos", "fade-up");

        Element aElement = new Element("a");
        aElement.attr("href", "images/" + image);
        aElement.attr("class", "d-block photo-item");
        aElement.attr("data-fancybox", "gallery");

        Element imgElement = new Element("img");
        imgElement.attr("src", "images/" + image);
        imgElement.attr("alt", "Image");
        imgElement.attr("class", "img-fluid");

        Element divElement2 = new Element("div");
        divElement2.attr("class", "photo-text-more");

        Element span = new Element("span");
        span.attr("class", "icon icon-search");

        divElement.appendChild(aElement);
        aElement.appendChild(imgElement);
        aElement.appendChild(divElement2);
        divElement2.appendChild(span);

        return divElement;
    }

    /**
     * Generates an array with to be used in row of images
     *
     * @param m the amount of images in the row
     * @param n the size of the column
     * @return array to be used to distribute image sizes in a row
     */
    private int[] positions(int m, int n) {
        int[] imageProportions = new int[m];
        Arrays.fill(imageProportions, 2);
        for (int i = 0; i < (n - (2 * m)); i++) {
            int randInt = random.nextInt(m);
            imageProportions[randInt]++;
        }

        return imageProportions;
    }
}
