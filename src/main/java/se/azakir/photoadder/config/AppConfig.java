package se.azakir.photoadder.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "se.azakir.photoadder")
public class AppConfig {
}
