package se.azakir.photoadder.init;

import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

@Component
public class InitApp {

    public static final String APPLICATION_FILE = "./application.properties";
    public static final String PROJECT_PATH = "project.path";
    Properties properties;

    public InitApp() {
        init();
    }

    public void init() {
        File propertyfile = new File(APPLICATION_FILE);
        try {
            if (!propertyfile.exists()) {
                propertyfile.createNewFile();
            }
            properties = new Properties();
            properties.load(new FileInputStream(propertyfile.getAbsolutePath()));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets property from properties file
     *
     * @param key name of property
     * @return value of property
     */
    public String getProperty(String key) {
        return properties.getProperty(key);
    }

    /**
     * Adds property to property file
     *
     * @param key      name of the property
     * @param property value of the property
     */
    public void addProperty(String key, String property) {
        properties.setProperty(key, property);
        try {
            properties.store(new FileOutputStream(APPLICATION_FILE), null);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
